Noah Assignment 3 code review:
Own Code Reviewer
Checklist
1. From 18 warnings down to 8 warnings. No errors. Removed unnecessary part of coding 
2. Pass all tests
3. No spelling mistakes in comments and incorporate some more comments.
4. Delete some useless part and make the code look clean

Code Review Checklist for Code Reviewer
Implementation
* Does this code change do what it is supposed to do?
Yes, it catches problem with input
* Can this solution be simplified?
Yes, already delete some unnecessary code
* Does this change add unwanted compile-time or run-time dependencies?
No, it does not because input remain the same.
* Was a framework, API, library, service used that should not be used?
Yes, removed two imports that is unnecessary. 
* Was a framework, API, library, service not used that could improve the solution?
Possibly yes, we can import io library to read the input files.  
* Is the code at the right abstraction level?
No, everything is in main and two function outside the main.
* Is the code modular enough?
two functions outside the main could be modular and can be used for other programs.
* Are there any best practices, design patterns or language-specific patterns that could substantially improve this code?
There could be more abstraction for the design.
* Does this code follow Object-Oriented Analysis and Design Principles, like the Single Responsibility Principle, Open-close principle, Liskov Substitution Principle, Interface Segregation, Dependency Injection?
No, too much code is inside main.
Logic Errors and Bugs
* Can you think of any use case in which the code does not behave as intended?
Providing more than one input files for the program.
* Can you think of any inputs or external events that could break the code?
If Java package was changed, the code may be broken.
Error Handling and Logging
* Is error handling done the correct way?
Yes, all errors have been throwed.
* Are error messages user-friendly?
Yes, everything is friendly.
* Are there enough log events and are they written in a way that allows for easy debugging?
Yes, the easy debugging is allowed.
Usability and Accessibility
* Is the proposed solution well designed from a usability perspective?
Not if users are unfamiliar with what should be input.
* Is the proposed solution (UI) accessible?
No, there are no instructions for the user.
Testing and Testability
* Is the code testable?
Yes, it is.
* Does it have enough automated tests (unit/integration/system tests)?
No.
* Are there some test cases, input or edge cases that should be tested in addition?
Yes, it does not test the number of characters in the entire file.
Dependencies
* If this change requires updates outside of the code, like updating the documentation, configuration, readme files, was this done?
Readme was not updated with instructions.


Security and Data Privacy
* If code deals with user input, does it address security vulnerabilities such as cross-site scripting, SQL injection, does it do input sanitization and validation?
Users are free to provide anything in the input. input sanitization was done to meet the standard requirements but not for security.
Performance
* Do you see any potential to improve the performance of the code?
I have used a quick sort and hash map to improve the performance.
Readability
* Was the code easy to understand?
Yes, it is easy to understand.
* Which parts were confusing to you and why?
No, there is none.
* Can the readability of the code be improved by smaller methods?
Yes, some codes are duplicated. 
* Is the code located in the right file/folder/package?
No, test code should be included inside the test folder.
* Are there redundant comments?
No, there are not too many comments.
* Would more comments make the code more understandable?
Yes
* Is there any commented out code?
          No there is not.





Pancheng Qu Assignment 3 code review:
Own Code Reviewer
Checklist
1. From 6 warnings down to 4 warnings. No errors. Removed unnecessary part of coding 
     2. Pass all tests
     3. No spelling mistakes in comments and incorporate some more comments.
     4. Delete some useless part and make the code look clean
Code Review Checklist for Code Reviewer
Implementation
* Does this code change do what it is supposed to do?
Yes, it catches problem with input
* Can this solution be simplified?
Yes, there are too many linked lists and copies of linked lists which can be simplified.
* Does this change add unwanted compile-time or run-time dependencies?
No, it does not because input remains the same.
* Was a framework, API, library, service used that should not be used?
No, all imports are necessary. 
* Was a framework, API, library, service not used that could improve the solution?
Probably no, because every other package is not related to this assignment.  
* Is the code at the right abstraction level?
No, everything is in main and two function outside the main.
* Is the code modular enough?
No, there is no function outside the main. Everything is in the main.
* Are there any best practices, design patterns or language-specific patterns that could substantially improve this code?
There could be more abstraction and common function for the design.
* Does this code follow Object-Oriented Analysis and Design Principles, like the Single Responsibility Principle, Open-close principle, Liskov Substitution Principle, Interface Segregation, Dependency Injection?
No, all the code is inside main.
Logic Errors and Bugs
* Can you think of any use case in which the code does not behave as intended?
Providing a input without the delimiter “:”
* Can you think of any inputs or external events that could break the code?
If Java package was changed, the code may be broken.
Error Handling and Logging
* Is error handling done the correct way?
No, there should be more defensive coding.
* Are error messages user-friendly?
No, there are no error messages.
* Are there enough log events and are they written in a way that allows for easy debugging?
Yes, the easy debugging is allowed.
Usability and Accessibility
* Is the proposed solution well designed from a usability perspective?
Not if users are unfamiliar with what should be input.
* Is the proposed solution (UI) accessible?
No, there are no instructions for the user.
Testing and Testability
* Is the code testable?
Yes, it is.
* Does it have enough automated tests (unit/integration/system tests)?
No.
* Are there some test cases, input or edge cases that should be tested in addition?
Yes, it does not test the number of characters in the entire file.
Dependencies
* If this change requires updates outside of the code, like updating the documentation, configuration, readme files, was this done?
Readme was not updated with instructions.
Security and Data Privacy
* If code deals with user input, does it address security vulnerabilities such as cross-site scripting, SQL injection, does it do input sanitization and validation?
Users are free to provide anything in the input. Input sanitization was done to meet the standard requirements but not for security.
Performance
* Do you see any potential to improve the performance of the code?
I used a hashmap and different loop to improve the performance.
Readability
* Was the code easy to understand?
Yes, it is easy to understand.
* Which parts were confusing to you and why?
No, there is none.
* Can the readability of the code be improved by smaller methods?
Yes, some codes are duplicated. 
* Is the code located in the right file/folder/package?
No, test code should be included inside the test folder.
* Are there redundant comments?
No, there are not too many comments.
* Would more comments make the code more understandable?
Yes
* Is there any commented out code?
          No there is not.