# Code Review Checklist

# Description - Issues and Defects to Review in Code

# Defect Type

- [] Comments/Documentation: Incorrect or obsolete comments or information messages
- [] Syntax: Functions and Variables functions are not clear based on naming used
- [] Inputs: Incorrect reading of correct inputs or handling of incorrect inputs.
- [] Checking: Wrong or missing error messages or warnings, failure to sanitize input,  inadequate checks
- [] Data: Inefficient data structure or mismatch in data types used
- [] Logic: Incorrect logic of program or functions
- [] Packages: Incorrect or unneeded packages being imported
- [] Environment: Development environment support system problems such as IDE, design system, compiler version, test harness or cases
- [] Abstraction: Code is being repeated and should be abstracted into it's own function/method/class
- [] Modularity: Code is not modular enough and could be improved
- [] SOLID: Program code violates one of the principles of SOLID programming
- [] Errors: Are errors being handled correctly (or at all?) Should the program handle the issue instead of throwing an error or vice versa
- [] Usability: Errors or messages to the user are not clear. Program is not usable or clear to user as to how it should be used
- [] README: Documentation or README file has not been updated to match current code base
- [] Readability: Code is not readable for another programmer reviewing/editing

# Defect found during:
- [] Planning
- [] Development
- [] Testing
- [] Other (describe)

# Defect removed during:
- [] Planning
- [] Development
- [] Testing
- [] Other (describe)

